use {
    std::{
        path::{
            Path,
            PathBuf,
        },
        fs::{
            OpenOptions,
            // File,
            // read_dir,
        },
        io::{
            stdout,
            // stderr,
            Write,
            Read,
        },
        env::current_dir,
        sync::{
            mpsc::{
                channel,
                Sender,
                Receiver,
            },
            Arc,
            atomic::{ 
                AtomicU64,
                Ordering,
            },
        },
    },
    // rayon::prelude::*,
    clap::{
        crate_version,
        Arg,
        App,
    },
    sha2::{
        // digest::Output,
        Digest,
        Sha256,
    },
    generic_array::{
        // typenum::Unsigned,
        // ArrayLength,
        GenericArray,
        typenum::{
            U4,
            // U8,
            U16,
            // U32,
        },
    },
    murmur3::{
        murmur3_32,
        murmur3_x64_128,
        murmur3_x86_128,
    },
    hex,
    serde::{
        Serialize,
    },
    num_cpus,
    regex::{ RegexSet },
};

#[derive(Clone, Copy, Debug)]
enum OutputFormat {
    Default,
    Json,
    CSV,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ChecksumMethod {
    Sha256,
    Murmur3x64,
    Murmur3x86,
    Murmur3_32,
}

struct Config {
    pub paths: Vec<PathBuf>,
    pub exclude_paths_matching: RegexSet,
    pub recurse: bool,
    pub status: bool,
    pub checksum_method: ChecksumMethod,
    pub output: Box<dyn std::io::Write>,
    pub output_format: OutputFormat,
    pub largest: Option<usize>,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            paths: vec![],
            exclude_paths_matching: RegexSet::empty(),
            checksum_method: ChecksumMethod::Sha256,
            recurse: false,
            status: false,
            output: Box::new(stdout()) as Box<dyn Write>,
            output_format: OutputFormat::Default,
            largest: None,
        }
    }
}

#[derive(Debug, Clone)]
enum Operation {
    ScanFile(PathBuf),
    Complete,
}

#[derive(Debug, Clone, Hash, PartialEq, PartialOrd, Eq, Serialize)]
struct ChecksumData {
    file_size: usize,
    digest: String,
}

#[derive(Debug, Clone, Serialize)]
struct ChecksumOperationResult {
    file: PathBuf,
    checksum: ChecksumData,
}

#[derive(Debug, Clone)]
enum TerminalUpdate {
    FoundFile(PathBuf),
    RejectedFile(PathBuf),
    ChecksumedFile(PathBuf),
    Complete,
}

fn parse_args() -> Config {
    let matches = App::new("duplicate_discovery")
        .version(crate_version!())
        .author("Teifion S. <icckleblackcat@kittens.uk.net>")
        .about("Finds duplicate files in the given path")
        .arg(Arg::with_name("path")
            .short("p")
            .long("path")
            .value_name("PATH")
            .help("Sets the path to search")
            .takes_value(true)
            .multiple(true))
        .arg(Arg::with_name("exclude")
            .short("x")
            .long("exclude")
            .value_name("EXCLUDE")
            .help("a regex that excludes matching paths from being searched")
            .takes_value(true)
            .multiple(true))
        .arg(Arg::with_name("recursive")
            .short("r")
            .long("recursive")
            .value_name("RECURSIVE")
            .help("Search subdirectories")
            .takes_value(false))
        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("OUTPUT")
            .help("Output to the given file")
            .takes_value(true))
        .arg(Arg::with_name("status")
            .short("s")
            .long("status")
            .value_name("STATUS")
            .help("Show progress as a single * for each file hashed & . for each file queued for processing")
            .takes_value(false))
        .arg(Arg::with_name("method")
            .short("m")
            .long("method")
            .value_name("METHOD")
            .help("Select the checksum method.  Valid inputs are 'sha256', 'murmur3', 'murmur3_128'.  Defaults to sha256")
            .takes_value(true))
        .arg(Arg::with_name("format")
            .short("f")
            .long("format")
            .value_name("FORMAT")
            .help("Set the output format.  Valid formats are 'default', 'json' & 'csv'.  Defaults to 'default'")
            .takes_value(true))
        .arg(Arg::with_name("largest")
            .short("l")
            .long("largest")
            .value_name("LARGEST")
            .help("Restricts output to the largest N files where N in an integer")
            .takes_value(true))
        // TODO: Implement this
        // .arg(Arg::with_name("exact")
        //     .short("e")
        //     .long("exact")
        //     .value_name("EXACT")
        //     .help("Check for an exact match.  This requires a byte comparison of files with matching hash and size.")
        //     .takes_value(false))
        .get_matches();

    // If we specified the multiple() setting we can get all the values
    let mut paths = vec![];
    if let Some(in_v) = matches.values_of("path") {
        for in_file in in_v {
            let path = PathBuf::from(in_file);
            paths.push(path);
        }
    }

    let mut exclude_matching = vec![];
    if let Some(in_v) = matches.values_of("exclude") {
        for in_regex in in_v {
            let expr: String = in_regex.into();
            exclude_matching.push(expr);
        }
    }
    let exclude_paths_matching: RegexSet = if exclude_matching.len() > 0 {
        RegexSet::new(&exclude_matching).expect("Failed to parse regular expressions")
    } else {
        RegexSet::empty()
    };

    if paths.len() == 0 {
        let path = current_dir().expect("Cannot read the current path, please specify one with --path \"/path/to/search/\"");
        paths.push(path);
    }

    let checksum_method = match matches.value_of("method") {
        Some("murmur3") => ChecksumMethod::Murmur3_32,
        Some("murmur3_128") => {
            if cfg!(x86_64) {
                ChecksumMethod::Murmur3x64
            } else {
                ChecksumMethod::Murmur3x86
            }
        },
        Some("sha256") => ChecksumMethod::Sha256,
        _ => ChecksumMethod::Sha256,
    };

    let output_format = match matches.value_of("format") {
        Some("default") => OutputFormat::Default,
        Some("json") => OutputFormat::Json,
        Some("csv") => OutputFormat::CSV,
        _ => OutputFormat::Default,
    };

    let output = match matches.value_of("output") {
        Some(op) => {
            Box::new(
                OpenOptions::new()
                    .write(true)
                    .create_new(true)
                    .open(op)
                    .expect(&format!("Failed to open output file {:?}.", op))
            ) as Box<dyn Write>
        },
        None => {
            Box::new(
                stdout()
            ) as Box<dyn Write>
        },
    };

    let recurse = matches.is_present("recursive");
    let status = matches.is_present("status");
    let largest = match matches.value_of("largest") {
        Some(v) => Some(v.parse::<usize>().expect("Failed to parse largest value as a number")),
        None => None,
    };

    Config {
        paths,
        exclude_paths_matching,
        recurse,
        status,
        output,
        output_format,
        checksum_method,
        largest,
    }
}

fn main() {

    // parse the program armuents for the config
    let config = parse_args();

    let min_size_for_processing = Arc::new(AtomicU64::new(0));

    // create the MPSC channels for inter0thread communication
    let (tx, rx) = channel::<Operation>();
    let (checksum_tx, checksum_rx) = channel::<ChecksumOperationResult>();
    let (terminal_tx, terminal_rx) = channel::<TerminalUpdate>();

    // create the terminal update thread if required
    if config.status {
        std::thread::spawn(move || {
            process_terminal(terminal_rx);
        });
    }

    // create the file processing thread and hand it the receiver channel
    // for messages indicating which files to checksum and the sender channel
    // so it can send notifications of messages to checksum
    let checksum_method = config.checksum_method;
    let term_tx = terminal_tx.clone();
    std::thread::spawn(move || {
        process_files(rx, checksum_tx, checksum_method, term_tx);
    });

    // create a thread to scan the directories for files & hand it the 
    // operation send channel handle so it can send files to the processing queue
    let operation_notifier = tx.clone();
    let paths = config.paths;
    let exclusions = config.exclude_paths_matching;
    let recurse = config.recurse;
    let term_tx = terminal_tx.clone();
    let min_size = min_size_for_processing.clone();
    std::thread::spawn(move ||{
        for path in paths {
            let _r = scan_dir(&path, exclusions.clone(), recurse, operation_notifier.clone(), term_tx.clone(), min_size.clone());
        }
        let _r = operation_notifier.send(Operation::Complete);
    });

    // create a hashtable to hold the list of resultant files
    // These are hashed by their ChecksumData & so should be unique
    // as long as the file is identical
    let mut hashtable = std::collections::HashMap::new();

    // let mut s_out = stderr();
    let mut file_count: u128 = 0;
    let start = std::time::SystemTime::now();
    // Now listen on the checksum result queue... this gets send a message
    // when a file has been checksummed and is closed when the task is complete.
    while let Ok(checksum_result) = checksum_rx.recv() {
        file_count += 1;
        // if config.status {
        //     let _r = s_out.write("*".as_bytes());
        // }
        if !hashtable.contains_key(&checksum_result.checksum) {
            hashtable.insert(checksum_result.checksum.clone(), vec![]);
        }
        let matches = hashtable.get_mut(&checksum_result.checksum);
        matches.unwrap().push(checksum_result.file);
        // calculate the minimum file size we are interested in
        if let Some(limit) = config.largest {
            let mut top_sizes = vec![];
            for key in hashtable.keys() {
                let fsize = key.file_size;
                // check if there are actual duplicates here
                if hashtable.get(key).unwrap_or(&vec![]).len() > 1 {
                    top_sizes.push(fsize);
                    // sort so the last element is the smallest
                    top_sizes.sort_unstable_by(|a, b| b.cmp(a));
                    // now remove the smallest if we have too many
                    top_sizes.truncate(limit);
                }
            }
            let smallest = *top_sizes.last().unwrap_or(&0);
            min_size_for_processing.store(smallest as u64, Ordering::Relaxed);
        }
    }
    let end = std::time::SystemTime::now();

    // output results:
    // if config.status {
    //     let _r = s_out.write("\n".as_bytes());
    // }
    let _r = terminal_tx.send(TerminalUpdate::Complete);

    // measure the time it took to do the job
    let duration = end.duration_since(start).unwrap_or(std::time::Duration::from_secs(1));
    let total_seconds = if duration.as_secs() > 0 { duration.as_secs() } else { 1 };
    let secs_per_file = total_seconds as f64 / file_count as f64;
    let files_per_sec = (file_count as f64 / total_seconds as f64) as u128;

    // calculate the total bytes processed
    let byte_count: u128 = hashtable.iter().map(|(k, v)| k.file_size as u128 * v.len() as u128 ).sum();
    let gb_processed = byte_count / (1000 * 1000 * 1000);
    let mb_processed = byte_count / (1000 * 1000);
    let mb_per_sec = (mb_processed as f64 / total_seconds as f64) as u128;

    // wait for the terminal thread to stop
    std::thread::sleep(std::time::Duration::from_secs(1));

    if let Some(max_reports) = config.largest {
        let mut top_sizes = vec![];
        for key in hashtable.keys() {
            let fsize = key.file_size;
            // check if there are actual duplicates here
            if hashtable.get(key).unwrap_or(&vec![]).len() > 1 {
                top_sizes.push(fsize);
                // sort so the last element is the smallest
                top_sizes.sort_unstable_by(|a, b| b.cmp(a));
                // now remove the smallest if we have too many
                top_sizes.truncate(max_reports);
            }
        }
        let smallest = *top_sizes.last().unwrap_or(&0);
        hashtable = hashtable.into_iter().filter(|(k, _v)| k.file_size >= smallest).collect();
    }

    match write_results(hashtable, config.output, config.output_format) {
        Ok(_) => {},
        Err(e) => {
            println!("Error writing output: {:?}", &e);
        },
    }

    println!("{:?} Files over {:?} seconds", &file_count, &duration);
    println!("{:?} seconds per file.", secs_per_file);
    println!("{:?} files per second.", files_per_sec);
    println!("{:?} GB at {:?} Mb per second", gb_processed, mb_per_sec);
}

fn write_results(results: std::collections::HashMap<ChecksumData, std::vec::Vec<std::path::PathBuf>>, output: Box<dyn Write>, format: OutputFormat) -> std::io::Result<()> {
    match format {
        OutputFormat::Default => {
            write_default(results, output)
        },
        OutputFormat::Json => {
            write_json(results, output)
        },
        OutputFormat::CSV => {
            write_csv(results, output)
        },
    }
}

fn write_default(results: std::collections::HashMap<ChecksumData, std::vec::Vec<std::path::PathBuf>>, mut output: Box<dyn Write>) -> std::io::Result<()> {
    for key in results.keys() {
        if let Some(val) = results.get(key) {
            if val.len() > 1 {
                let _r = output.write(format!("*** File Size: {:?} - HASH: {:?}\n", &key.file_size, &key.digest).as_bytes())?;
                for file in val {
                    let _r = output.write(format!("{:?}\n", file).as_bytes())?;
                }
                output.write("\n".as_bytes())?;
            }
        }
    }
    Ok(())
}

fn write_json(results: std::collections::HashMap<ChecksumData, std::vec::Vec<std::path::PathBuf>>, mut output: Box<dyn Write>) -> std::io::Result<()> {
    let mut output_text = format!("{{\n\t\"matches\": [\n");
    for key in results.keys() {
        if let Some(val) = results.get(key) {
            if val.len() > 1 {
                let hash = &key.digest;
                let size = &key.file_size;
                output_text.push_str(&format!("\t\t{{\n\t\t\t\"match\": {{\n\t\t\t\t\"hash\": {:?},\n\t\t\t\t\"size\": {:?},\n\t\t\t\t\"files\": [\n", hash, size));
                for file in val {
                    output_text.push_str(&format!("\t\t\t\t\t{:?},\n", file));
                }
                let _r = output_text.pop();
                let _r = output_text.pop();
                output_text.push_str("\n\t\t\t\t]\n\t\t\t}\n\t\t},\n");
            }
        }
    }
    let _r = output_text.pop();
    let _r = output_text.pop();
    output_text.push_str("\n\t]\n}\n");
    output.write(output_text.as_bytes())?;
    // let json = serde_json::to_string(&results)?;
    // output.write(json.as_bytes())?;
    Ok(())
}

fn write_csv(results: std::collections::HashMap<ChecksumData, std::vec::Vec<std::path::PathBuf>>, mut output: Box<dyn Write>) -> std::io::Result<()> {
    output.write(format!("HASH,SIZE,FILE\n").as_bytes())?;
    for key in results.keys() {
        if let Some(val) = results.get(key) {
            if val.len() > 1 {
                let hash = &key.digest;
                let size = &key.file_size;
                for file in val {
                    let _r = output.write(format!("{:?},{:?},{:?}\n", &hash, &size, &file).as_bytes())?;
                }
            }
        }
    }
    Ok(())
}

fn update_terminal(files_found: usize, file_rejected_count: usize, files_processed: usize) {
    use crossterm::{
        // execute,
        // queue, 
        QueueableCommand, 
        // ExecutableCommand,
        style::Print, 
        terminal::{ Clear, ClearType, size },
        cursor::{ MoveTo, },
    };

    let (width, _) = size().unwrap();
    let cols_per_percent = width as f64 / 100.0;
    let percent = (files_processed as f64 / files_found as f64) * 100.0;
    let progress = (percent * cols_per_percent) as usize;

    let prog = "█".repeat(progress);
    let rejected = format!("Rejected: {}", file_rejected_count);

    let _r = stdout()
        .queue(MoveTo(0,0)).unwrap()
        .queue(Clear(ClearType::CurrentLine)).unwrap()
        .queue(Print(format!("      Found: {}", files_found))).unwrap()
        .queue(MoveTo(width-rejected.len() as u16, 0)).unwrap()
        .queue(Print(rejected)).unwrap()
        .queue(MoveTo(0,1)).unwrap()
        .queue(Clear(ClearType::CurrentLine)).unwrap()
        .queue(Print(format!("  Processed: {}", files_processed))).unwrap()
        .queue(MoveTo(0,2)).unwrap()
        .queue(Clear(ClearType::CurrentLine)).unwrap()
        .queue(Print(prog)).unwrap()
        .flush();
}

fn process_terminal(rx: Receiver<TerminalUpdate>) {
    use crossterm::{execute, terminal};

    let mut output = stdout();

    let _r = execute!(
        output,
        terminal::Clear(terminal::ClearType::All),
    );

    let mut file_found_count = 0;
    let mut file_summed_count = 0;
    let mut file_rejected_count = 0;

    // let mut last_print: String = "".into();

    while let Ok(terminal_op) = rx.recv() {
        match terminal_op {
            TerminalUpdate::RejectedFile(_file_path) => {
                file_rejected_count = file_rejected_count + 1;
                update_terminal(file_found_count, file_rejected_count, file_summed_count);
            },
            TerminalUpdate::FoundFile(_file_path) => {
                file_found_count = file_found_count + 1;
                update_terminal(file_found_count, file_rejected_count, file_summed_count);
            },
            TerminalUpdate::ChecksumedFile(_file_path) => {
                file_summed_count = file_summed_count + 1;
                update_terminal(file_found_count, file_rejected_count, file_summed_count);
            },
            TerminalUpdate::Complete => {
                let _r = execute!(
                    output,
                    terminal::Clear(terminal::ClearType::All),
                );
                return
            }
        }
    }
}

fn process_files(rx: Receiver<Operation>, checksum_tx: Sender<ChecksumOperationResult>, method: ChecksumMethod, terminal_tx: Sender<TerminalUpdate>) {
    let cpu_count = num_cpus::get();
    let _r = rayon::ThreadPoolBuilder::default()
        .num_threads(cpu_count * 2)
        .build_global();
    // let mut s_out = stderr();
    'processing: loop {
        match rx.recv() {
            Ok(Operation::ScanFile(path)) => {
                let notifier = checksum_tx.clone();
                let term_notifier = terminal_tx.clone();
                // let _r = s_out.write(".".as_bytes());
                rayon::spawn(move ||{
                    match checksum_file(&path, method) {
                        Ok(checksum_data) => {
                            let _r = term_notifier.send(
                                TerminalUpdate::ChecksumedFile(path.clone())
                            );
                            let _r = notifier.send(
                                ChecksumOperationResult {
                                    file: path,
                                    checksum: checksum_data,
                                }
                            );
                        },
                        Err(_e) => {
                            // Error handling file
                            // TODO: we'll ignore this for now.
                            // maybe we should send a message to stderr
                        },
                    }
                });
            },
            Ok(Operation::Complete) => {
                break 'processing;
            },
            Err(e) => {
                let _r = std::io::stderr().write(format!("Error reading from processing channel: {:?}\n", &e).as_bytes());
                break 'processing;
            },
        }
    }
}

fn scan_dir(dir: &Path, exclusions: RegexSet, recurse: bool, file_notification_queue: Sender<Operation>, terminal_tx: Sender<TerminalUpdate>, min_file_size: Arc<AtomicU64>) -> std::io::Result<()> {
    let reader = dir.read_dir()?;
    for entry in reader {
        match entry {
            Ok(de) => {
                let de_path = de.path();
                let not_excluded = match de_path.into_os_string().into_string() {
                    Ok(de_path_str) => {
                        let match_count = exclusions.matches(&de_path_str).into_iter().count();
                        // stdout().write(format!("Match Count: {:?}\n", match_count).as_bytes());
                        match_count == 0
                    },
                    Err(_) => {
                        // We'll ignore this for now.. the basic problem is that we can't 
                        // get the path as a string to match against the regular expression.
                        // This shouldn't crop up except on filesystems with filenames
                        // that do not translate to utf-8
                        true
                    },
                };
                if not_excluded {
                    if let Ok(file_type) = de.file_type() {
                        if file_type.is_dir() {
                            if recurse {
                                let _r = scan_dir(&de.path(), exclusions.clone(), recurse, file_notification_queue.clone(), terminal_tx.clone(), min_file_size.clone());
                            }
                        } else if file_type.is_file() {
                            let sz = min_file_size.load(Ordering::Relaxed);
                            let do_processing = if sz > 0 {
                                de.metadata()
                                    .map_or(
                                        true,
                                        |v| v.len() as u64 >= sz,
                                    )
                            } else {
                                true
                            };
                            if do_processing {
                                let _r = terminal_tx.send(
                                    TerminalUpdate::FoundFile(PathBuf::from(&de.path()))
                                );
                                let _r = file_notification_queue.send(
                                    Operation::ScanFile(PathBuf::from(&de.path()))
                                );
                            } else {
                                let _r = terminal_tx.send(
                                    TerminalUpdate::RejectedFile(PathBuf::from(&de.path()))
                                );
                            }
                        } else {
                            // should be a symlink, either way, we don't care
                        }
                    }
                }
            },
            Err(_e) => {

            },
        }
    }
    Ok(())
}

// pub type ChecksumResult = GenericArray<u8, <sha2::Sha256 as Digest>::OutputSize>;
pub type ChecksumResult = u128;

fn checksum_murmur3_32<T: Read>(from: &mut T) -> std::io::Result<GenericArray<u8, U4>> {
    Ok(murmur3_32(from, 0)?.to_be_bytes().into())
}

fn checksum_murmur3_128_x64<T: Read>(from: &mut T) -> std::io::Result<GenericArray<u8, U16>> {
    Ok(murmur3_x64_128(from, 0)?.to_be_bytes().into())
}

fn checksum_murmur3_128_x86<T: Read>(from: &mut T) -> std::io::Result<GenericArray<u8, U16>> {
    Ok(murmur3_x86_128(from, 0)?.to_be_bytes().into())
}

fn checksum_sha256<T: Read>(from: &mut T) -> std::io::Result<GenericArray<u8, <sha2::Sha256 as Digest>::OutputSize>> {
    let mut hasher = Sha256::default();
    let mut buf = [0u8; 16384];
    'reader: loop {
        let bytes_read = from.read(&mut buf)?;
        hasher.update(&buf[0..bytes_read]);
        if bytes_read < 16384 {
            break 'reader;
        }
    }
    Ok(hasher.finalize())
}


fn checksum_file(path: &Path, checksum_method: ChecksumMethod) -> std::io::Result<ChecksumData> {
    let mut f = OpenOptions::new().write(false).read(true).create(false).open(path)?;
    let file_size = path.metadata()?.len() as usize;
    let digest = match checksum_method {
        ChecksumMethod::Sha256 => {
            let result = checksum_sha256(&mut f)?;
            hex::encode(result)
        },
        ChecksumMethod::Murmur3_32 => {
            let result = checksum_murmur3_32(&mut f)?;
            hex::encode(result)
        },
        ChecksumMethod::Murmur3x64 => {
            let result = checksum_murmur3_128_x64(&mut f)?;
            hex::encode(result)
        },
        ChecksumMethod::Murmur3x86 => {
            let result = checksum_murmur3_128_x86(&mut f)?;
            hex::encode(result)
        },
    };
    // let result = murmur3_x64_128(&mut f, 0)?;
    Ok(ChecksumData {
        file_size,
        digest,
    })
}

// #[cfg(test)]
// mod test {
//     use super::*;

//     #[test]
//     fn checksum_dir() {
//         let path = Path::new("./src");

//         scan_dir(&path, true);
//     }
// }
